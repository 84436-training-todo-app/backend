from uuid import uuid4
from django.db import models

class List(models.Model):
    title = models.CharField(blank=False, null=False, max_length=150)

    # Use UUID as secondary key (unique=True);
    # UUID as primary key (primary_key=True) might have performance consequences,
    # see https://stackoverflow.com/a/3944247
    uuid = models.UUIDField(unique=True, default=uuid4, editable=False)

    def __str__(self):
        return self.title
