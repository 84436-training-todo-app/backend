from todo_app.common.serializers import CleanModelSerializer
from .model import List

class ListSerializer(CleanModelSerializer):
    class Meta:
        model = List
        fields = ('uuid', 'title')
