from django.urls import path
from .view import (
    api_list_all_view,
    api_list_detail_view,
    api_list_create_view,
    api_list_update_view,
    api_list_delete_view
)

app_name = 'todo'

urlpatterns = [
    path('', api_list_all_view, name="list_all"),
    path('create/', api_list_create_view, name="list_create"),
    path('<uuid>/', api_list_detail_view, name="list_one"),
    path('<uuid>/update/', api_list_update_view, name="list_update"),
    path('<uuid>/delete/', api_list_delete_view, name="list_delete"),
]
