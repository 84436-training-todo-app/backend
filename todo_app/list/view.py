#from django.shortcuts import render
from rest_framework import status
from rest_framework.response import Response
from rest_framework.decorators import api_view

from todo_app.todo.model import Todo
from .model import List
from .serializer import ListSerializer

@api_view(['GET'])
def api_list_all_view(request):
    lists = List.objects.all()
    serializer = ListSerializer(lists, many=True)
    return Response(serializer.data)

@api_view(['GET'])
def api_list_detail_view(request, uuid):
    try:
        list_item = List.objects.get(uuid=uuid) # Fetch one list from models with corresponding ID
    except List.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)
    serializer = ListSerializer(list_item) # Serialize that, then output
    return Response(serializer.data)

@api_view(['PUT'])
def api_list_update_view(request, uuid):
    try:
        list_item = List.objects.get(uuid=uuid)
    except List.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)    
    serializers = ListSerializer(list_item, data=request.data)
    data = {}
    if serializers.is_valid():
        serializers.save()
        data['success'] = 'Updated successfully'
        return Response(data=data)
    return Response(serializers.errors, status=status.HTTP_400_BAD_REQUEST)

@api_view(['DELETE'])
def api_list_delete_view(request, uuid):
    try:
        list_item = List.objects.get(uuid=uuid)
    except List.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)    

    operation = list_item.delete()
    data = {}

    if operation:
        # Delete all todo object with matching list UUID
        # Since we do not (properly) specify relationship between two models in the first place,
        # we have to do this by hand. *sigh*
        todo_items = Todo.objects.filter(listUUID=uuid)
        if todo_items:
            todo_items.delete()
        data['success'] = "Deleted successfully"
    
    else:
        data['failure'] = "Failed to delete list"
    return Response(data=data)

@api_view(['POST'])
def api_list_create_view(request):
    list_item = List()
    serializers = ListSerializer(list_item, data=request.data)
    if serializers.is_valid():
        serializers.save()
        return Response(serializers.data, status=status.HTTP_201_CREATED)
    return Response(serializers.errors, status=status.HTTP_400_BAD_REQUEST)
