from todo_app.common.serializers import CleanModelSerializer
from .model import Todo

class TodoSerializer(CleanModelSerializer):
    class Meta:
        model = Todo
        fields = ('uuid', 'listUUID', 'title', 'desc', 'timestamp', 'isDone')
