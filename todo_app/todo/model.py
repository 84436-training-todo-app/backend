from uuid import uuid4
from django.db import models
from todo_app.common.utils import get_default_list_uuid

class Todo(models.Model):
    listUUID = models.CharField(blank=False, max_length=36, default=get_default_list_uuid()) # 36 = length of UUIDv4
    title = models.CharField(blank=False, max_length=150)
    desc = models.TextField(blank=True)
    timestamp = models.CharField(blank=True, max_length=len("YYYY-MM-DDTHH:mm"))
    isDone = models.BooleanField(blank=False, default=False)

    # Use UUID as secondary key (unique=True);
    # UUID as primary key (primary_key=True) might have performance consequences,
    # see https://stackoverflow.com/a/3944247
    uuid = models.UUIDField(unique=True, default=uuid4, editable=False)

    def __str__(self):
        return self.title
