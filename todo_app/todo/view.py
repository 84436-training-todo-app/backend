#from django.shortcuts import render
from rest_framework import status
from rest_framework.response import Response
from rest_framework.decorators import api_view

from .model import Todo
from .serializer import TodoSerializer

@api_view(['GET'])
def api_todo_all_view(request):
    todos = Todo.objects.all()
    serializer = TodoSerializer(todos, many=True)
    return Response(serializer.data)

@api_view(['GET'])
def api_todo_detail_view(request, uuid):
    try:
        todo_item = Todo.objects.get(uuid=uuid) # Fetch one todo from models with corresponding ID
    except Todo.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)
    serializer = TodoSerializer(todo_item) # Serialize that, then output
    return Response(serializer.data)

@api_view(['PUT'])
def api_todo_update_view(request, uuid):
    try:
        todo_item = Todo.objects.get(uuid=uuid)
    except Todo.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)    
    serializers = TodoSerializer(todo_item, data=request.data)
    data = {}
    if serializers.is_valid():
        serializers.save()
        data['success'] = 'Updated successfully'
        return Response(data=data)
    return Response(serializers.errors, status=status.HTTP_400_BAD_REQUEST)

@api_view(['DELETE'])
def api_todo_delete_view(request, uuid):
    try:
        todo_item = Todo.objects.get(uuid=uuid)
    except Todo.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)    
    operation = todo_item.delete()
    data = {}
    if operation:
        data['success'] = "Deleted successfully"
    else:
        data['failure'] = "Failed to delete task"
    return Response(data=data)

@api_view(['POST'])
def api_todo_create_view(request):
    todo_item = Todo()
    serializers = TodoSerializer(todo_item, data=request.data)
    if serializers.is_valid():
        serializers.save()
        return Response(serializers.data, status=status.HTTP_201_CREATED)
    return Response(serializers.errors, status=status.HTTP_400_BAD_REQUEST)
