from django.urls import path
from .view import (
    api_todo_all_view,
    api_todo_detail_view,
    api_todo_create_view,
    api_todo_update_view,
    api_todo_delete_view
)

app_name = 'todo'

urlpatterns = [
    path('', api_todo_all_view, name="todo_all"),
    path('create/', api_todo_create_view, name="todo_create"),
    path('<uuid>/', api_todo_detail_view, name="todo_one"),
    path('<uuid>/update/', api_todo_update_view, name="todo_update"),
    path('<uuid>/delete/', api_todo_delete_view, name="todo_delete"),
]
