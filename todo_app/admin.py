from django.contrib import admin
from .list.model import List
from .todo.model import Todo

# Register models in order to be shown on the Admin page
admin.site.register(Todo)
admin.site.register(List)
