from typing import OrderedDict
from rest_framework import serializers

# https://stackoverflow.com/a/44024512
class CleanModelSerializer(serializers.ModelSerializer):
    def to_representation(self, value):
        repr_dict = super(CleanModelSerializer, self).to_representation(value)
        return OrderedDict((k, v) for k, v in repr_dict.items() 
                           if v not in [None, [], '', {}])
