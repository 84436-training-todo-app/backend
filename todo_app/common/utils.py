from todo_app.list.model import List

# Get UUID for list "Default"
# 
# Yes, you've just found another dirty hack.
# This highly depends on the fixtures (see todo_app/fixtures/*) being imported correctly
# on first migration.
def get_default_list_uuid():
    try:
        return str(List.objects.get(id=1).uuid)
    except:
        return ''
