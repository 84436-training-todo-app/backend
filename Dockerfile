# Start with a Python base image

### BEGIN 🚨 GIANT RED NEON SIGN
# Alpine Linux, as usual, uses musl as the primary libc.
# One of the faster solutions that
#   (a) does not require compiling psycopg2 from scratch (bloating the image), and
#   (b) resorting to multi-stage builds
# is to just just a glibc-powered image.
# So... Debian slim it is.
### END 🚨 GIANT RED NEON SIGN

#FROM python:alpine
FROM python:3-slim

# Copy the application
WORKDIR /app
COPY . .

# Install dependencies
RUN pip install -r requirements.txt

# Perform database migration for the first time, then load default data
RUN python manage.py makemigrations
RUN python manage.py migrate
RUN python manage.py loaddata default

# Expose the port
# This is actually ignored by Heroku --
# a random port will be assigned to the dyno this app is running from.
EXPOSE $PORT

# Run the application
CMD python manage.py runserver 0.0.0.0:$PORT
