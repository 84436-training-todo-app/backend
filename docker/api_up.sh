#!/usr/bin/env bash

# network=host: exposed ports (-p) will be ignored,
# so we don't need to specify it here.
docker run \
    -d \
    --network host \
    --name todo-api-instance \
    todo-api
