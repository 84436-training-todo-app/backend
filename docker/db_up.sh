#!/usr/bin/env bash

docker run \
    -d \
    -p 5432:5432 \
    -v todo-api-db:/var/lib/postgresql/data \
    -e POSTGRES_PASSWORD=postgres \
    --name todo-api-db \
    postgres:alpine
